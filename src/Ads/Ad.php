<?php

namespace AutoAvaliar\B2B\Ads;

class Ad
{

    private $initialValue;
    private $status = 1;

    /**
     * @param float $initialValue
     */
    public function setInitialValue(float $initialValue): void
    {
        $this->initialValue = $initialValue;
        $this->status       = 2;
    }

    /**
     * @return float
     */
    public function getMinValue(): float
    {
        return $this->initialValue;
    }

    /**
     * @param float $bid
     * @return float
     * @throws AdException
     */
    public function sendBid(float $bid): float
    {
        if ($bid < 100) {
            throw new AdException('Valor de lance precisa ser maior que o incremento minimo (100 bolsonaros).');
        }
        return $this->initialValue += $bid;
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function buy(int $userId): bool
    {

        if ($this->checkOpenPayments($userId)) {
            return false;
        }

        if ($this->status == 3) {
            return false;
        }
        $this->status = 3;
        return true;
    }

    public function checkOpenPayments($userId)
    {
        return ($userId%2);
    }

    /**
     * @return int
     */
    public function status(): int
    {
        return $this->status;
    }

}
