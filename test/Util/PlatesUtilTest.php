<?php

namespace AutoAvaliar\B2BTest\Util;

use PHPUnit\Framework\TestCase;

/**
 * Class PlateUtilTest
 * @package AutoAvaliar\B2BTest\Util
 */
class PlatesUtilTest extends TestCase
{
    protected function assertPreConditions(): void
    {
        /**
         * A classe PlatesUtil deverá ser implementada
         * @namespace AutoAvaliar\B2B\Util
         */
        $this->assertDirectoryExists('../src/Util/', 'Namespace (/src/Util/) não existe.' );
        $this->assertTrue(
            class_exists('AutoAvaliar\B2B\Util\PlatesUtil'),
            'Classe não existe.'
        );

        /**
         * Esta função deverá retornar a placa como uma string limpa, ou false quando não for válida, ex:
         *  quando aaa-1000 o resultado esperado é AAA1000 (maiusculo, sempre 7 caracteres)
         *  quando aaa 1030 o resultado esperado é AAA1030
         *  quando Abc1000 o resultado esperado é ABC1000
         *  quando ATG**80 o resultado esperado é false
         *
         *  Verifique mais exemplos em `PlateUtilTest::platesProvider()`
         *
         * @ps Utilizar padrão de placas => 3 letras + 4 numeros
         */
        $this->assertTrue(
            method_exists('AutoAvaliar\\B2B\\Util\\PlatesUtil', 'validate'),
            'Método `validate()` não implementado'
        );

        /**
         * Este método `countRange()` deverá retornar quantas possibilidades de placas existem entre as que serão fornecidas
         */
        $this->assertTrue(
            method_exists('AutoAvaliar\\B2B\\Util\\PlatesUtil', 'countRange'),
            'Método `countRange()` não implementado'
        );
    }


    /**
     * DataProvider for testIfPlateIsValid
     * @return array
     */
    public function platesProvider()
    {
        return [
            ['aaa0000', 'AAA0000'],
            ['a0a0000', false],
            ['zzzA000', false],
            ['aba-0070', 'ABA0070'],
            ['a5a-0000', false],
            ['Avb8700', 'AVB8700'],
            ['FXJ-8700', 'FXJ8700'],
            ['A7b8700', false],
            ['ATG8700', 'ATG8700'],
            ['ATG87-0', false],
        ];
    }

    /**
     * @param $plate
     * @param $expected
     * @dataProvider platesProvider
     */
    public function testIfPlateIsValid($plate, $expected)
    {
        $plates = new \AutoAvaliar\B2B\Util\PlatesUtil();
        $this->assertEquals($expected, $plates->validate($plate));
    }


    /**
     * DataProvider for testIfPlateIsValid
     * @return array
     */
    public function rangesProvider()
    {
        return [
            ['AAA0000', 'AAA0000', 1], // inclusive, pois é uma placa valida neste range!
            ['AAA0000', 'AAA0001', 2],
            ['AAA0001', 'AAA0000', 2],
            ['AAA0000', 'AAA0003', 4],
            ['AAA0000', 'AAA9999', 10000],
            ['AAA0000', 'AAB0000', 10001],
            ['FFZ6754', 'MKJ7647', 48460894],
            ['AAA0000', 'ZZZ9999', 175760000],
        ];
    }

    /**
     * @param $plate1
     * @param $plate2
     * @param $expected
     * @throws \Exception
     *
     * @dataProvider rangesProvider
     */
    public function testPlateRanges($plate1, $plate2, $expected)
    {
        $plates = new \AutoAvaliar\B2B\Util\PlatesUtil();
        $this->assertEquals($expected, $plates->countRange($plate1, $plate2));
    }



    /**
     * DataProvider for testPlateErrorRanges
     * @return array
     */
    public function rangesErrorProvider()
    {
        return [
            ['AA10000', 'AAA0000'],
            ['AAA0000', 'AA-0001'],
            ['AAA0001', 'AA_0000'],
            ['AA^0001', 'AA_0000'],
        ];
    }

    /**
     * Testar placas incorretas
     * @dataProvider rangesErrorProvider
     */
    public function testPlateErrorRanges($plate1, $plate2)
    {
        $this->expectException('\Exception');


        $plates = new \AutoAvaliar\B2B\Util\PlatesUtil();
        $plates->countRange($plate1, $plate2);
    }
}
