<?php

namespace AutoAvaliar\B2BTest\Vehicle;

use AutoAvaliar\B2B\Ads\{Ad, AdException};
use PHPUnit\Framework\TestCase;

class AdTest extends TestCase
{

    public function assertPreConditions(): void
    {
        $this->assertTrue(class_exists('AutoAvaliar\B2B\Ads\Ad'), 'Classe não encontrada!');
        $this->assertTrue(method_exists('AutoAvaliar\B2B\Ads\Ad', 'sendBid'), 'Método `sendBid()` não encontrado');
        $this->assertTrue(method_exists('AutoAvaliar\B2B\Ads\Ad', 'setInitialValue'), 'Método `setInitialValue()` não encontrado');
        $this->assertTrue(method_exists('AutoAvaliar\B2B\Ads\Ad', 'buy'), 'Método `buy()` não encontrado');
    }

    /**
     * @test
     */
    public function testSetInitialValue(): void
    {
        $ad = new Ad();
        $ad->setInitialValue(48000);

        $this->assertEquals(48000, $ad->getMinValue());
        $this->assertEquals(2, $ad->status());

    }

    /**
     * @test
     */
    public function testSendBid(): void
    {
        $ad = new Ad();
        $ad->setInitialValue(48000);

        $this->assertEquals(48100, $ad->sendBid(100));
        $this->assertEquals(48100, $ad->getMinValue());
    }

    /**
     * @test
     */
    public function testSendBidWithNegativeValue(): void
    {
        $ad = new Ad();
        $ad->setInitialValue(48000);

        $this->expectException(AdException::class);

        $ad->sendBid(-100);
    }

    /**
     * @test
     */
    public function testSendBidLessThanMinimumIncrement(): void
    {
        $ad = new Ad();
        $ad->setInitialValue(48000);

        $this->expectException(AdException::class);

        $ad->sendBid(50);
    }


    /**
     * Data for sendBid
     */
    public function valuesBidProvider()
    {
        return [
            [48000, 100, 48100],
            [36000, 150, 36150],
            [39000, 200, 39200],
        ];
    }

    /**
     * @param $initial
     * @param $bid
     * @throws AdException
     * @dataProvider valuesBidProvider
     */
    public function testSendBidWithDataProvider($initial, $bid, $expected): void
    {
        $ad = new Ad();
        $ad->setInitialValue($initial);
        $ad->sendBid($bid);

        $this->assertEquals($expected, $ad->getMinValue());
    }

    /**
     * @test
     */
    public function testBuy(): void
    {
        $ad = new Ad();
        $this->assertEquals(false, $ad->buy(5)); // nao compre
        $this->assertEquals(true, $ad->buy(4));
        $this->assertEquals(3, $ad->status());
    }
}
