# targets
.PHONY: __header
__header:
	@echo
	@echo "  |- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|"
	@echo "  |  ____ ___  ____                  _                         _ _              |"
	@echo "  | |  _ \__ \|  _ \      /\        | |         /\            | (_)             |"
	@echo "  | | |_) | ) | |_) |    /  \  _   _| |_ ___   /  \__   ____ _| |_  __ _ _ __   |"
	@echo "  | |  _ < / /|  _ <    / /\ \| | | | __/ _ \ / /\ \ \ / / _' | | |/ _' | '__|  |"
	@echo "  | | |_) / /_| |_) |  / ____ \ |_| | || (_) / ____ \ V / (_| | | | (_| | |     |"
	@echo "  | |____/____|____/  /_/    \_\__,_|\__\___/_/    \_\_/ \__,_|_|_|\__,_|_|     |"
	@echo "  |                                                                             |"
	@echo "  |                         business to business                                |"
	@echo "  |                    https://b2b.autoavaliar.com.br                           |"
	@echo "  |- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|"
	@echo

.PHONY: default
default: help

.PHONY: help
help: __header
	@echo
	@echo "The most common targets are:"
	@echo
	@echo "- start                Builds, (re)creates, starts, and attaches to containers for a service."
	@echo "- stop                 Stops containers and removes containers, networks, volumes, and images created by up."
	@echo "- restart	          Guess what :-)"
	@echo "- logs                 Show container logs."
	@echo "- interact             Interact within the container."
	@echo "- help                 Display this help message"
	@echo

.PHONY: restart
restart: stop start

.PHONY: start
start: __header
	@docker-compose up

.PHONY: stop
stop: __header
	@docker-compose down

.PHONY: logs
logs:
	@docker container logs tdd-phpunit --follow

.PHONY: interact
interact:
	@docker container exec -it tdd-phpunit bash
